import { put, call, all, takeEvery, select, takeLatest } from 'redux-saga/effects';
import Action from './action';
import api from "../api";
import {navigate} from "../helper/HistoryHelper";

export default function* () {
  yield all([
    takeLatest(Action.Types.FETCH_LOGIN, function* ({auth}) {
      const result = yield call(api.fetchLogin, auth);
      console.log("@@@@@ result", result);
      localStorage.userToken = "token";
      // yield put(Action.Creators.fetchLogin({auth}))
    }),
    takeLatest(Action.Types.FETCH_REGISTER, function* ({auth}) {
      const result = yield call(api.fetchRegister, auth);
      console.log("@@@@@ result", result);
      localStorage.userToken = "token";
      // yield put(Action.Creators.fetchLogin({auth}))
    }),


    takeLatest(Action.Types.USER_LOGOUT, function* () {
      localStorage.removeItem("userToken");
      yield put(Action.Creators.updateState({isAuth: false}))
    }),

    takeLatest(Action.Types.REFRESH_USER, function* ({token}) {
      if(token){
        yield put(Action.Creators.updateState({isAuth: true}))
      } else {
        yield put(Action.Creators.updateState({isAuth: false}))
      }
    }),

    takeLatest(Action.Types.FETCH_POSTS, function* () {

      const posts = yield call(api.fetchPost);

      console.log("@@@@@ posts", posts);
      yield put(Action.Creators.updateState({posts}));
    }),

    takeLatest(Action.Types.CREATE_POST, function* ({post}) {
      const posts = yield call(api.createPost, post);
      yield put(Action.Creators.updateState({latestPost: posts}));
      navigate('/pages/posts');
    }),

    takeLatest(Action.Types.SEARCH_POST, function* ({param}) {
      const post = yield call(api.searchPost, param);
      yield put(Action.Creators.updateState({postDetail: post}));
    }),

    takeLatest(Action.Types.FETCH_USERS, function* () {


      const users = yield call(api.fetchUser);

      console.log("@@@@@ users", users);
      yield put(Action.Creators.updateState({users}))
    }),

    takeLatest(Action.Types.FETCH_USER_DETAIL, function* ({userId}) {

      const userDetail = yield call(api.fetchUserDetail, userId);

      yield put(Action.Creators.updateState({userDetail}))
    }),

    takeLatest(Action.Types.HANDLE_SIDEBAR, function* ({status}) {
      yield put(Action.Creators.updateState({isSidebarOpen: status}))
    }),


  ]);
}

