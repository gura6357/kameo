const Types = {

  FETCH_POSTS: "FETCH_POSTS",
  CREATE_POST: "CREATE_POST",
  SEARCH_POST: "SEARCH_POST",


  FETCH_LOGIN: "FETCH_LOGIN",
  FETCH_REGISTER: "FETCH_REGISTER",


  USER_LOGOUT: "USER_LOGOUT",
  FETCH_USERS: "FETCH_USERS",
  REFRESH_USER: "REFRESH_USER",
  FETCH_USER_DETAIL: "FETCH_USER_DETAIL",

  UPDATE_STATE: "UPDATE_STATE",
  HANDLE_SIDEBAR: "HANDLE_SIDEBAR",

};



const Creators = {
  updateState: props => ({
    type: Types.UPDATE_STATE,
    props
  }),

  createPost: post => ({
    type: Types.CREATE_POST,
    post
  }),

  fetchPosts: () => ({
    type: Types.FETCH_POSTS,
  }),

  searchPost: param => ({
    type: Types.SEARCH_POST,
    param,
  }),

  fetchUsers: users => ({
    type: Types.FETCH_USERS,
    users
  }),

  handleSidebar: (status) => ({
    type: Types.HANDLE_SIDEBAR,
    status
  }),
  refreshUser: token => ({
    type: Types.REFRESH_USER,
    token
  }),
  fetchLogin: auth => ({
    type: Types.FETCH_LOGIN,
    auth
  }),
  fetchRegister: auth => ({
    type: Types.FETCH_REGISTER,
    auth
  }),

  userLogout: () => ({
    type: Types.USER_LOGOUT,
    isAuth: false
  }),

  fetchUserDetail: userId => ({
    type: Types.FETCH_USER_DETAIL,
    userId
  }),
};


const ActionFactory = () => {
  let Action = {};
  Action.Types = Types;
  Action.Creators = Creators;

  return Action;
};

const Action = ActionFactory();

export default Action;


