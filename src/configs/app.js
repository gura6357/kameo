const DEV = "development";
const STAGING = "staging";
const PRODUCTION = "production";

export const EnvironmentTypes = {
    DEV,
    STAGING,
    PRODUCTION,
};

export const ENV = (() => {
    switch (process.env.REACT_APP_ENV) {
        case "production":
            return PRODUCTION;
        case "staging":
            return STAGING;
        default:
            return DEV;
    }
})();

export const DEBUG = ENV === DEV;
