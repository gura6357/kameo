import React, {useState, useEffect} from 'react';
import Header from "../components/Header";
import _ from 'lodash';
import {connect} from "react-redux";
import ImageCard from "../components/ImageCard";

function Category(props) {

    const {
        match,
        userCategories = [],
        users = [],
    } = props;


    console.log("@@ props", props);
    return (
        <div className="Category">
            <div className="container">
                <div className="search">
                    <input type="text"/>
                </div>

                <div className="h2">Categories</div>
                <div className="content">
                    <nav>
                        {
                            _.map(userCategories, (item, i) => <CategoryName key={i} {...item}/>)
                        }
                    </nav>
                    <div className="userList">
                        {
                            _.map(users, (user, i) => <div key={i} className="item"><ImageCard {...user}/></div>)
                        }
                    </div>
                </div>

            </div>


        </div>
    )
}

function CategoryName(props) {
    const {
        name,
        userCount,
    } = props;
    console.log("@@@@@ props", props);

    return (
        <div className={"CategoryName"}>
            {name}
            <span className="count">({userCount})</span>
        </div>
    )
}


export default connect(state => ({...state}), dispatch => ({dispatch}))(Category);
