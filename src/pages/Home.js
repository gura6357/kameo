import React, {useState, useEffect} from 'react';
import {Link} from "react-router-dom";
import _ from "lodash";
import {connect} from "react-redux";
import ImageCard from "../components/ImageCard";
import VideoCard from "../components/VideoCard";

function Home(props) {

    const {
        users = [],
        cameoVideos = [],
        reactionVideos = []
    } = props;

    console.log("@@ users", users);
    return (
        <div className="Home">
            <section className="Visual">
                <video autoPlay loop
                       poster="https://d3el26csp1xekx.cloudfront.net/static/assets/HomepageVideov2-thumb.jpg"
                       preload="auto">
                    <source src="https://d3el26csp1xekx.cloudfront.net/static/assets/HomepageVideov2.mp4"
                            type="video/mp4"/>
                </video>
                <div className="videoFilter"/>

                <div className="text">
                    <img src="https://d3el26csp1xekx.cloudfront.net/staticDir/logo_white.svg" alt="Cameo"/>
                    <Link to="/c/featured" className="btn btn-dark">
                        Browse our
                        talent
                    </Link>
                    <h1>Book personalized video shoutouts from your favorite people</h1>
                </div>
            </section>

            <section className="Featured">
                <div className="container">
                    <div className="text">
                        <p>
                            <i className="fa fa-star"/>
                            Featured
                        </p>
                        <h2 className="h2">Fantasy Football is Here</h2>
                        <p>
                            Break out your lucky jersey, sink into the couch, and glue your eyes to the TV. Football.
                            Is. BACK!
                            And we’ve got some of the hottest NFL stars on our roster. Only on Cameo will you find
                            record-breaking wide-receivers, all-star running backs, and legendary quarterbacks.
                        </p>
                        <p>Get Ray Lewis or James White to trash talk your fantasy opponent. Or ask Antonio Brown if he
                            has any sleepers heading into your next matchup. Win fantasy before fantasy even starts.</p>
                        <a className="btn btn-dark">Fantasy Football's Page</a>
                    </div>
                    <div className="thumb">
                        <Link to="/">
                            <img
                                src="https://starboard-media.s3-us-west-2.amazonaws.com/miscellaneous/homepages/football-homepage1.png"
                                alt=""/>
                        </Link>
                    </div>
                </div>
            </section>

            <section className="NewThisWeek">
                <h2 className="section-title">New this week</h2>
                <div className="userList">
                    {
                        _.map(users, (user, i) => <div key={i} className="item"><ImageCard categories={false}
                                                                                           label={'New'} {...user}/>
                        </div>)
                    }
                </div>
            </section>

            <section className="CameoVideos">
                <div className="container">
                    <h2 className="section-title">Featured Cameo videos</h2>
                    <div className="videos">
                        {
                            _.map([{}, {}, {}], (video, i) => (
                                <div key={i} className="item col-sm-4 col-xs-12">
                                    <div className="inner">
                                        <VideoCard {...video}/>
                                        <p className="ellipse ellipse-3">
                                            Book
                                            <a href="/bethennyfrankel">
                                                <strong className="_3P2Z564CzzXEwx2ktFnlZ9">Bethenny Frankel</strong>
                                            </a>
                                            to learn how to be the ultimate skinny girl this summer
                                        </p>
                                        <a className="btn btn-cameo-light" href="/bethennyfrankel">Book Bethenny Frankel</a>
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </section>

            <section className="ReactionVideos">
                <div className="container">
                    <h2 className="section-title">Reaction videos</h2>
                    <p>Post your own and tag us on Facebook/Instagram @cameo and on Twitter @bookcameo!</p>
                    <div className="videos">
                        {
                            _.map([{}, {}, {}], (video, i) => (
                                <div key={i} className="item col-sm-4 col-xs-12">
                                    <div className="inner">
                                        <VideoCard {...video}/>
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </section>


        </div>
    )
}

export default connect(state => ({...state}), dispatch => ({dispatch}))(Home);
