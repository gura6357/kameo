import React, {useState, useEffect, Component} from 'react';
import {Link, Redirect, withRouter} from 'react-router-dom';
import {Switch, Route} from 'react-router';
import {connect} from "react-redux";
import {HISTORY} from "./helper/HistoryHelper";
import Action from "./redux/action";
import './scss/style.scss';
import Home from "./pages/Home";
import Pages from './pages';
import Category from "./pages/Category";
import Header from "./components/Header";

function App(props) {

    const {
        dispatch,
        location,
    } = props;

    const currentPath = location.pathname;

    useEffect(() => {
        dispatch(Action.Creators.updateState({currentPath}));
        dispatch(Action.Creators.handleSidebar(false));
    }, [location.pathname]);


    const isRootPath = currentPath === '/';
    console.log("@@@@@ props", props);

    return (
        <div className="App">
            <Header label={!isRootPath && "Browse Talent"} isPageHome={isRootPath}/>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/c/:name" component={Category} />
                <Redirect exact to={'/'}/>
            </Switch>
        </div>

    );
}

export default connect(state => ({...state}), dispatch => ({dispatch}))(withRouter(App));
