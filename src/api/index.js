import {URL} from '../helper/ConstsHelper';
import FetchHelper from "../helper/FetchHelper";

const api = {
  createPost: async post => {
    console.log("@@@@@ post", post);
    return await FetchHelper.fetchJsonPost(`${URL.API_BASE_URL}/post`, post);
  },

  fetchPost: async() => {
    console.log("@@@@@ fetchPost api", `${URL.API_BASE_URL}/post`);
    return await FetchHelper.fetchJsonGet(`${URL.API_BASE_URL}/post`);
  }
};


export default api;
