import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Provider} from 'react-redux';
import store from "./redux/store";
import Action from "./redux/action";
import {BrowserRouter} from 'react-router-dom';
import {version, versionCode} from '../package.json';
import {ENV} from './configs/app';

// console.log(`Version: ${version}, Code: ${versionCode}, Environment: ${ENV}`);

// 주소표시줄에 직접입력시 처리
// store.dispatch(Action.Creators.updateState({currentPath: HISTORY.location.pathname}));
// store.dispatch(Action.Creators.refreshUser(localStorage.userToken));

ReactDOM.render(
    <BrowserRouter>
        <Provider store={store}>
            <App />
        </Provider>
    </BrowserRouter>
    , document.getElementById('root'));


