export const KEY_CODES = {
  ENTER: 13,
};

const ID = 'uid';
const USER_ID = 'userId';
const USER_LOGIN_ID = 'userLoginId';
const USER_NAME = 'userName';

export const CookieKeys = {ID, USER_ID, USER_NAME};


export const API_BASE_URL = process.env.REACT_APP_API_BASE_URL;
export const STATIC_BASE_URL = process.env.REACT_APP_STATIC_BASE_URL;
export const PUBLIC_BASE_URL = process.env.REACT_APP_PUBLIC_BASE_URL;


export const URL = {API_BASE_URL, STATIC_BASE_URL, PUBLIC_BASE_URL};
