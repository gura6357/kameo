import { createBrowserHistory, createHashHistory } from 'history';

const HISTORY = createBrowserHistory();
// const HISTORY = createHashHistory();

const navigate = url => {
  HISTORY.push(url);
};

const navigateReplace = url => {
  HISTORY.replace(url);
};

export {HISTORY, navigate, navigateReplace};