import React, { useState, useEffect } from 'react';
import {version} from '../../package.json';

function Footer (props) {

  const {} = props;

  return (
          <div className="Footer">
            VERSION: {version}
          </div>
      )
}

export default Footer;