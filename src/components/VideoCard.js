import React, {useState, useEffect} from 'react';

function VideoCard(props) {

    const {} = props;

    return (
        <div className="VideoCard">
            <video>
                <source src="https://d3el26csp1xekx.cloudfront.net/v/no-wm-g0bhbZtUx.mp4" type="video/mp4"/>
            </video>
            <img className="preview"
                 src="https://starboard-media.s3.amazonaws.com/v/no-wm-thumb-g0bhbZtUx-00001.jpg"
                 alt="Video player preview"
            />
            <div className="icon-play">
                <img
                    className="_3i_XRXKYAQRX0dKnCJx-Qt"
                    src="https://d3el26csp1xekx.cloudfront.net/static/assets/video-cards-2.0/play.svg"
                    alt="play icon"
                />
            </div>
        </div>
    )
}

export default VideoCard;
