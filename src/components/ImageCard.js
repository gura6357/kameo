import React, {useState, useEffect} from 'react';
import {Link} from "react-router-dom";
import _ from 'lodash';

function ImageCard(props) {

    const {
        price,
        label,
        name,
        profession,
        imageUrl,
        role,
        categories = [],
    } = props;

    return (
        <div className="ImageCard">
            <div className="thumb">
                <Link to="">
                    <img src={imageUrl} alt="" style={{minHeight: "100%"}}/>
                {
                    price &&
                    <div className="price">${price}</div>
                }
                {
                    label &&
                    <div className="label">
                        <span>{label}</span>
                    </div>
                }
                </Link>
            </div>
            <div className="desc">
                {
                    profession &&
                    <Link to="#!" className="profession">{profession}</Link>
                }
                {
                    name &&
                    <div className="name"><Link to="#!">{name}</Link></div>
                }
                {
                    categories &&
                    <div className="categories">
                        {
                            _.map(categories.split(',').splice(0,3), (c, i) => <Link to="#!" key={i} className="ct">{c}</Link>)
                        }
                    </div>
                }
            </div>
        </div>
    )
}

export default ImageCard;


