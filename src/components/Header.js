import React, { useState, useEffect } from 'react';
import {Link} from "react-router-dom";
import cn from 'classnames';

function Header (props) {

  const {
      isPageHome,
      label
  } = props;

  return (
          <div className={cn("Header", {isPageHome})}>
            <div className="container">
                <h1 className="logo">
                    <Link to="/">
                        <img src="https://d3el26csp1xekx.cloudfront.net/staticDir/logo_icon_white.svg" alt="cameo"/>
                        {label}
                    </Link>
                </h1>

                <nav>
                    <div className="navItem">Signup</div>
                    <div className="navItem">Login</div>
                </nav>
            </div>


          </div>
      )
}

export default Header;
