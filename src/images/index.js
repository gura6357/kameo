import m_magazine_gnb_menu from './m-magazine-gnb-menu.svg';
import circle_half_line from './circle-half-line.svg';
import favorite from './favorite.svg';

const Images = {
  m_magazine_gnb_menu,
  circle_half_line,
  favorite
};


export default Images;